<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/style.css'; ?>?3">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/responsive.css'; ?>?3">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">

	<!-- Google Webfonts -->
	<link href='http://fonts.googleapis.com/css?family=Didact+Gothic' rel='stylesheet' type='text/css'>

</head>
<body>
	<div id="front_page" class="container-fluid">
		<div class="center">
			<img class="logo" src="<?php echo get_template_directory_uri() . '/images/logo.jpg'; ?>" />
			<p class="intro">
				Have you been constidering purchasing a Kangen ionizer, liked the water, but are searching for a more affordable option that is equal or superior?
			</p>
		</div>
		<div class="row">
			<div class="col-md-6 center">
				<img src="<?php echo get_template_directory_uri() . '/images/product_shot_top.jpg'; ?>" />
			</div>
			<div class="col-md-6 pad-32-med">
				<h1 class="light-header center-med">YOU FOUND IT!</h1>
				<p class="med-font">Ayana Water is proud to offer the world’s leading water ionizing machine – VITALITY</p>
				<p class="med-font">The Ayana Vitalitenturyty employs the latest in Water Ionization technology with its Active Ionic Disks. After 8 years of development and testing, you will now now have a Water Ionizer that you can use WORRY FREE Read On....</p>
			</div>
		</div>
		<div class="row blue-row">
			<h1>Download your FREE E-Book AND Get $100 towards an Ayana VITALITY!</h1>
			<div class="col-md-7">
				<img src="<?php echo get_template_directory_uri() . '/images/gift-cert-ebook.png'; ?>" />
			</div>
			<div class="col-md-5">
				<form>
					<input type="text" name="name" class="name" />
					<input type="text" name="phone" class="phone" />
					<input type="text" name="email" class="email" />
				</form>
			</div>
		</div>

	</div>
</body>
</html>